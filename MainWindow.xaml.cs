﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MongoDB.Driver;
using MongoDB.Bson;   
using MongoDB.Shared;   
using MongoDB.Bson.Serialization.Attributes;
using System.Threading;
using System.Data;

namespace poc_.netcore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window 
    {
        public MainWindow()
        {
            InitializeComponent();
            Task task = LeerUsuarios();
        }
        public async Task LeerUsuarios()
        {
            try
            {
                List<Usuarios> list = await ConnectionMongoDB.collectionUsuarios.AsQueryable().ToListAsync<Usuarios>();
                dtusuarios.ItemsSource = list;
                Usuarios u = (Usuarios)dtusuarios.Items.GetItemAt(0);
                txtApellido.Text = u.apellido;
                txtNombre.Text   = u.nombre;
                txtID.Text = u.Id.ToString();
            }
            catch (System.Exception)
            {
                
                throw;
            }

        }
        private async void  btn_create(object sender, RoutedEventArgs e)
        {
            try
            {
                Usuarios usuario = new Usuarios(txtNombre.Text, txtApellido.Text);
                await UsuariosController.CrearUsuarios<Usuarios>(usuario);
                var list = await UsuariosController.RefrescarUsuarios<Usuarios>();
                dtusuarios.ItemsSource = list;                  
            }
            catch (System.Exception)
            {
                
                throw;
            }
        
        }
        private async void  btn_refresh(object sender, RoutedEventArgs e)
        {         
            try
            {
              var list = await UsuariosController.RefrescarUsuarios<Usuarios>();
              dtusuarios.ItemsSource = list;    
            }
            catch (System.Exception)
            {            
                throw;
            }  

        }
        private void btn_update(object sender, RoutedEventArgs e)
        {
            try
            {
                var updateDef = Builders<Usuarios>.Update.Set("nombre",txtNombre.Text).Set("apellido",txtApellido.Text);
                ConnectionMongoDB.collectionUsuarios.UpdateOne( b => b.Id == ObjectId.Parse(txtID.Text), updateDef);
            }
            catch (System.Exception)
            {               
                throw;
            }
        
  
        }
        private void btn_delete(object sender, RoutedEventArgs e)
        {
            try
            {
                ConnectionMongoDB.collectionUsuarios.DeleteOne(u => u.Id == ObjectId.Parse(txtID.Text));    
            }
            catch (System.Exception)
            {
                
                throw;
            }
            
        }
        private async void dtusuarios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

           try
            {
                await Task.Delay(TimeSpan.FromSeconds(5));
                Usuarios u = (Usuarios)dtusuarios.SelectedItem;

                if (u!=null)
                {
                   
                    txtApellido.Text = u.apellido;
                    txtNombre.Text = u.nombre;
                    txtID.Text = u.Id.ToString();

                }

            }
            catch (System.Exception)
            {
                
                throw;
            }

        }
    }
}
