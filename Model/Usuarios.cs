using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace poc_.netcore
{

    public class Usuarios
    {
        public Usuarios(string nombre, string apellido)
        {
            this.nombre = nombre;
            this.apellido = apellido;
        }

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("nombre")]
        public string nombre { get; set; }
        
        [BsonElement("apellido")]
        public string apellido { get; set; }


    }
    
}