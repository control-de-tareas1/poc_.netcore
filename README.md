# Proof of Concept _MongoDb_  _To_ _WPF_

## Instrucciones de Ejecución

 * Compilar Aplicación 

    ```bash
    dotnet build
    ```
 * Ejecutar Aplicación 

    ```bash
    dotnet run
    ```

### 1. Crear Instancia de Mongo Atlas 

### 2. Establecer Conexión con Base de datos

### 3. Crear Proyecto WPF .Net Core

### 4. CRUD 
A-. Crear
B-. Leer
C-. Eliminar
D-. Actualizar


