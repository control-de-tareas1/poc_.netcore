using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace poc_.netcore
{
    
    public class UsuariosController
    {
        public static async Task CrearUsuarios<T>(Usuarios usuarios)
        {              
             await ConnectionMongoDB.collectionUsuarios.InsertOneAsync(usuarios);             
        }

        public static async Task<List<Usuarios>> RefrescarUsuarios<T>( )
        {     
             List<Usuarios> list = await ConnectionMongoDB.collectionUsuarios.AsQueryable().ToListAsync<Usuarios>();
             return list;         
        }

       

    }
}